<?php

if( ! defined( 'ABSPATH' ) ) { exit; }

define("BASEPATH", 1);

/**
 * paylink Main Class
 */


class PLK_WC_Payment_Gateway extends WC_Payment_Gateway{

    /**
     * Constructor
     *
     * @return void
     */

    public function __construct(){
        $this->base_url = 'https://paylink.ng/';
        $this->id = 'paylink';
        $this->icon = plugins_url('assets/images/Paylink-Logo.png', PLK_WC_PLUGIN_FILE);
        $this->has_fields         = false;
        $this->method_title       = __( 'Paylink', 'plk-payments' );
        $this->method_description = __( 'Paylink Allows you to accept payment from clients without giving out your bank details', 'plk-payments' );
        $this->supports = array(
          'products',
        );

        $this->init_form_fields();
        $this->init_settings();

        $this->title        = $this->get_option( 'title' );
        $this->description  = $this->get_option( 'description' );
        $this->username      = $this->get_option( 'username' );
        $this->enabled      = $this->get_option( 'enabled' );
        $this->auto_complete_order = get_option('autocomplete_order');
        $this->go_live      = $this->get_option( 'go_live' );
        $this->payment_style = $this->get_option( 'payment_style' );
        $this->logging_option = $this->get_option('logging_option');
        $this->country ="";


        add_action( 'woocommerce_receipt_' . $this->id, array($this, 'receipt_page'));


        if ( is_admin() ) {
          add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        }

        $this->load_scripts();
  
    }


    /**
     * Initial gateway settings form fields
     *
     * @return void
     */

    public function init_form_fields() {

        $this->form_fields = array(
  
          'enabled' => array(
            'title'       => __( 'Enable/Disable', 'plk-payments' ),
            'label'       => __( 'Enable Paylink Payment Gateway', 'plk-payments' ),
            'type'        => 'checkbox',
            'description' => __( 'Enable Paylink Payment Gateway as a payment option on the checkout page', 'plk-payments' ),
            'default'     => 'no',
            'desc_tip'    => true
          ),
          'go_live' => array(
            'title'       => __( 'Mode', 'plk-payments' ),
            'label'       => __( 'Live mode', 'plk-payments' ),
            'type'        => 'checkbox',
            'description' => __( 'Check this box if you\'re using your live keys.', 'plk-payments' ),
            'default'     => 'no',
            'desc_tip'    => true
          ),
          'logging_option' => array(
            'title'       => __( 'Disable Logging', 'plk-payments' ),
            'label'       => __( 'Disable Logging', 'plk-payments' ),
            'type'        => 'checkbox',
            'description' => __( 'Check this box if you\'re disabling logging.', 'plk-payments' ),
            'default'     => 'no',
            'desc_tip'    => true
          ),
          'title' => array(
            'title'       => __( 'Payment method title', 'plk-payments' ),
            'type'        => 'text',
            'description' => __( 'Optional', 'plk-payments' ),
            'default'     => 'Paylink'
          ),
          'username' => array(
            'title'       => __( 'Username', 'plk-payments' ),
            'type'        => 'text',
            'description' => __( 'The username of the merchant to receive payment', 'plk-payments' ),
            'default'     => ''
          ),
          'description' => array(
            'title'       => __( 'Payment method description', 'plk-payments' ),
            'type'        => 'text',
            'description' => __( 'Optional', 'plk-payments' ),
            'default'     => 'Powered by Paylink: Accepts Mastercard, Visa, Verve, Discover, AMEX, Diners Club and Union Pay.'
          ),
          'payment_style' => array(
            'title'       => __( 'Payment Style on checkout', 'plk-payments' ),
            'type'        => 'select',
            'description' => __( 'Optional - Choice of payment style to use. Either inline or redirect. (Default: inline)', 'plk-payments' ),
            'options'     => array(
              'inline' => esc_html_x( 'Popup(Keep payment experience on the website)', 'payment_style', 'plk-payments' ),
              'redirect'  => esc_html_x( 'Redirect',  'payment_style', 'plk-payments' ),
            ),
            'default'     => 'inline'
          ),
          'autocomplete_order'               => array(
            'title'       => __( 'Autocomplete Order After Payment', 'plk-payments' ),
            'label'       => __( 'Autocomplete Order', 'plk-payments' ),
            'type'        => 'checkbox',
            'class'       => 'wc-plk-autocomplete-order',
            'description' => __( 'If enabled, the order will be marked as complete after successful payment', 'plk-payments' ),
            'default'     => 'no',
            'desc_tip'    => true,
          ),
        );
  
    }

    /**
     * Checkout receipt page
     *
     * @return void
     */
    public function receipt_page( $order ) {

      $order = wc_get_order( $order );
      
      echo '<p>'.__( 'Thank you for your order, please click the <b>Make Payment</b> button below to make payment. You will be redirected to a secure page where you can enter you card details or bank account details. <b>Please, do not close your browser at any point in this process.</b>', 'plk-payments' ).'</p>';
      echo '<a class="button cancel" href="' . esc_url( $order->get_cancel_order_url() ) . '">';
      echo __( 'Cancel order &amp; restore cart', 'plk-payments' ) . '</a> ';
      echo '<button class="button alt  wc-forward" id="plk-pay-now-button">Make Payment</button> ';
      

    }

    /**
     * Loads (enqueue) static files (js & css) for the checkout page
     *
     * @return void
     */
    public function load_scripts() {

      if ( ! is_checkout_pay_page() ) return;
      $username = $this->username;
      //$payment_options = $this->payment_options;
       
      if( $this->payment_style == 'redirect'){
        wp_enqueue_script( 'plk_inline_js', $this->base_url . 'cdn/paylink.checkout.js', array(), '1.0.0', true );
      }

      wp_enqueue_script( 'plk_js', plugins_url( 'assets/js/plk.js', PLK_WC_PLUGIN_FILE ), array( 'jquery' ), '1.0.0', true );

      if ( get_query_var( 'order-pay' ) ) {
        
        $order_key = urldecode( $_REQUEST['key'] );
        $order_id  = absint( get_query_var( 'order-pay' ) );
        $cb_url = WC()->api_request_url( 'PLK_WC_Payment_Gateway' ).'?paylink_id='.$order_id;
       
        if( $this->payment_style == 'inline'){
          wp_enqueue_script( 'plk_inline_js', $this->base_url . 'cdn/paylink.checkout.js', array(), '1.0.0', true );
          $cb_url = WC()->api_request_url('PLK_WC_Payment_Gateway');
        }

        $order     = wc_get_order( $order_id );
        
        $txnref    = "WOOC_" . $order_id . '_' . time();
        $txnref    = filter_var($txnref, FILTER_SANITIZE_STRING); //sanitizr=e this field
       
        if (version_compare(WOOCOMMERCE_VERSION, '2.7.0', '>=')){
              $amount    = $order->get_total();
              $email     = $order->get_billing_email();
              $currency     = $order->get_currency();
              $main_order_key = $order->get_order_key();
        }else{
            $args = array(
                'name'    => $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
                'email'   => $order->get_billing_email(),
                'contact' => $order->get_billing_phone(),
            );
            $amount    = $order->get_total();
            $main_order_key = $order->get_order_key();
            $email     = $order->get_billing_email();
            $currency     = $order->get_currency();
        }
        
        // $amount    = $order->order_total;
        // $email     = $order->billing_email;
        // $currency     = $order->get_order_currency();
        
        //set the currency to route to their countries
        /**switch ($currency) {
            case 'KES':
              $this->country = 'KE';
              break;
            case 'GHS':
              $this->country = 'GH';
              break;
            case 'ZAR':
              $this->country = 'ZA';
              break;
            case 'TZS':
              $this->country = 'TZ';
              break;
            
            default:
              $this->country = 'NG';
              break;
        }**/
        
        $country  = 'NG';
        $payment_style  = $this->payment_style;

        if ( $main_order_key == $order_key ) {

          $payment_args = compact( 'amount', 'email', 'txnref', 'username', 'currency', 'country','cb_url','payment_style');
          $payment_args['desc']   = filter_var($this->description, FILTER_SANITIZE_STRING);
          $payment_args['title']  = filter_var($this->title, FILTER_SANITIZE_STRING);
          // $payment_args['logo'] = filter_var($this->modal_logo, FILTER_SANITIZE_URL);
          $payment_args['firstname'] = $order->get_billing_first_name();
          $payment_args['lastname'] = $order->get_billing_last_name();
          $payment_args['contact'] = $order->get_billing_phone();
          //$payment_args['barter'] = $this->barter;
        }

        update_post_meta( $order_id, '_plk_payment_txn_ref', $txnref );

      }

      wp_localize_script( 'plk_js', 'plk_payment_args', $payment_args );


    }


    /**
     * Process payment at checkout
     *
     * @return int $order_id
     */
    public function process_payment( $order_id ) {

      $order = wc_get_order( $order_id );
  
      return array(
        'result'   => 'success',
        'redirect' => $order->get_checkout_payment_url( true )
      );

    }

  


}
