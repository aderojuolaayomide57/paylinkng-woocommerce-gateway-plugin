<?php

/*
Plugin Name: Paylink WooCommerce Payment Gateway
Plugin URI: https://rave.flutterwave.com/
Description: Official WooCommerce payment gateway for Paylink.
Version: 1.0
Author: Ayomide 
Author URI: http://developer.flutterwave.com
License: MIT License
WC requires at least:   3.0.0
WC tested up to:        4.9.2
*/


define( 'PLK_WC_PLUGIN_FILE', __FILE__ );
define( 'PLK_WC_DIR_PATH', plugin_dir_path( PLK_WC_PLUGIN_FILE ) );


function plk_woocommerce_paylink_init() {

  if ( !class_exists( 'WC_Payment_Gateway' ) ) return;

  require_once( PLK_WC_DIR_PATH . 'includes/class.plk_wc_payment_gateway.php' );

  add_filter('woocommerce_payment_gateways', 'plk_woocommerce_add_paylink_gateway', 99 );
}
add_action('plugins_loaded', 'plk_woocommerce_paylink_init', 99);


  /**
   * Add the Settings link to the plugin
   *
   * @param  Array $links Existing links on the plugin page
   *
   * @return Array  Existing links with our settings link added
   * 
   */

  function plk_plugin_action_links( $links ) {

    $paylink_settings_url = esc_url( get_admin_url( null, 'admin.php?page=wc-settings&tab=checkout&section=paylink' ) );
    array_unshift( $links, "<a title='Paylink Settings Page' href='$paylink_settings_url'>Settings</a>" );

    return $links;

  }
  add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'plk_plugin_action_links' );






  /**
   * Add the Gateway to WooCommerce
   *
   * @param  Array $methods Existing gateways in WooCommerce
   *
   * @return Array  Gateway list with our gateway added
   * 
   */

  function plk_woocommerce_add_paylink_gateway($methods) {

    if ( class_exists( 'WC_Payment_Gateway_CC' ) ) {

      $methods[] = 'PLK_WC_Payment_Gateway';
    }else{
      $methods[] = 'PLK_WC_Payment_Gateway';
    }

    return $methods;

  }
